#!/usr/bin/python
__author__ = 'Tony Xu'

# Import libraries
import difflib
import sys
import getopt       
import difflib

# Store input and output file names
ifile1_name = ''
ifile2_name = ''
ifile1 = ''
ifile2 = ''

def main():

    # Get file names from command line 
    get_filename()
 
    ifile1 = open(ifile1_name, 'r')
    ifile2 = open(ifile2_name, 'r')
    

    # Compare two files
    d = difflib.Differ()
    diff = d.compare(ifile1.readlines(), ifile2.readlines())


    for line in diff:
        if line.startswith('-'):
            print("[1.txt]: %s " % line)
        elif line.startswith('+'):
            print("[2.txt]: %s " % line)

    ifile1.close()
    ifile2.close()
    
def get_filename():
    global ifile1_name, ifile2_name
    try:
	    opts, args = getopt.getopt(sys.argv[1:], "f:g:", ["file1=","file2="])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-f", "--file1"):
            ifile1_name = arg
        elif opt in ("-g", "--file2"):
            ifile2_name = arg

def usage():
	print("./compare.py -f <file1_name> -g <file2_name>\n")
	print("./compare.py --file1 <file1_name> --file2 <file2_name>\n")

if __name__ == "__main__":
    main()
