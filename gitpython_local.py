#!/usr/bin/python

import re
import os
import sys
import shutil
import git
from git import *

""" headcommit = repo.head.commit
headcommit.hexsha
'207c0c4418115df0d30820ab1a9acd2ea4bf4431'

headcommit.parents
(<git.Commit "a91c45eee0b41bf3cdaad3418ca3850664c4a4b4">,)

headcommit.tree
<git.Tree "563413aedbeda425d8d9dcbb744247d0c3e8a0ac">

headcommit.author
<git.Actor "Michael Trier <mtrier@gmail.com>">

headcommit.authored_date        # seconds since epoch
1256291446

headcommit.committer
<git.Actor "Michael Trier <mtrier@gmail.com>">

headcommit.committed_date
1256291446

headcommit.message
'cleaned up a lot of test information. Fixed escaping so it works with
subprocess.'
"""

repo = ''
r_repo = ''
remote_url = "https://tonylixu@bitbucket.org/tonylixu/file_compare.git"
DIR_NAME = "/tmp/file_compare"

def diff():
    global repo
    hcommit = repo.head.commit
    idiff = hcommit.diff()
    for line in idiff:
        print line

    tdiff = hcommit.diff('HEAD~1')
    for line in tdiff:
        print line

def main():
    global repo, r_repo, remote_url
    # Get local repo info
    repo = Repo("/home/lxu/Android_studio_projects/DCLibraryManagementSystem")
    assert repo.bare == False

    if os.path.isdir(DIR_NAME):
        shutil.rmtree(DIR_NAME)

    os.mkdir(DIR_NAME)

    Repo.clone_from(remote_url, DIR_NAME)
    r_repo = Repo(DIR_NAME)
    headcommit = r_repo.iter_commits('master', max_count=100)
    for line in headcommit:
        print line.message

    #t = repo.head.commit.tree
    #for line in t:
    #    print line

    # Read first 10 commit
    headcommit = repo.iter_commits('Integration', max_count=100)

    for line in headcommit:
        m = re.match(r"Merge", line.message, re.IGNORECASE)
        if m:
            pass
            #print line.hexsha, "-", line.message

    #diff()

if __name__ == "__main__":
    main()
